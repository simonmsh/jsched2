package jSched2;

//import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.*;

import java.util.Date;
import java.util.LinkedList;
import java.util.Map;
import java.util.List;

import jdk.nashorn.internal.parser.JSONParser;

public class listingsBean {
    private String filter="";
    private String channel="all";

    public String getFilter ()
    {
        return this.filter;
    }    

    public void setFilter (final String filter)
    {
        this.filter = filter.toLowerCase();
    }    
    
    public String getChannel ()
    {
        return this.channel;
    }    

    public void setChannel (final String channel)
    {
        this.channel = channel;
    }    
    
    private class Broadcast {
    	String start;
    	String channel;
    	String yr;
    	String title;
    	String epTitle;
    	String desc;
    	String typ;
    }
    
    public String getListings ()
    {
        String answer = "";
        
    	try {
        URL url = new URL("http://sgurr-tv-listings-4.azurewebsites.net/service");
        InputStreamReader reader = new InputStreamReader(url.openStream(), "UTF-8");

        Type broadcastsType = new TypeToken<List<Broadcast>>() {}.getType();
        
        Gson gson = new Gson();
        
        List<Broadcast> broadcasts = gson.fromJson(reader, broadcastsType);

        // using the deserialized object
        for (Broadcast broadcast : broadcasts) {
        	if (
        			(this.channel.equalsIgnoreCase("all") || broadcast.channel.toLowerCase().startsWith(this.channel.toLowerCase())) && 
    				(
        			this.filter == "" || 
        			broadcast.title.toLowerCase().contains(this.filter) ||
        			broadcast.epTitle.toLowerCase().contains(this.filter) ||
        			broadcast.desc.toLowerCase().contains(this.filter)
        			)
    				)
        	{
        	answer += "<tr>" +
        			"<td>" + broadcast.start + "</td>" +
        			"<td>" + broadcast.channel + "</td>" +
        			"<td>" + broadcast.title + "<br/><small>" + broadcast.epTitle + "</small><br/>" + broadcast.yr + "<br/>" + broadcast.typ + "</td>" +
        			"<td>" + broadcast.desc + "</td>" +
					"</tr>\n";
        	}
        }
    	} catch (Exception ex) {
    		answer += ex.toString() + "\n";
    	}
    	//answer = answer + "filter is " + filter;
    	//answer = answer + "channel=" + channel + "=";
    		
        return answer;
    }
}
